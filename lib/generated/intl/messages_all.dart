// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that looks up messages for specific locales by
// delegating to the appropriate library.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:implementation_imports, file_names, unnecessary_new
// ignore_for_file:unnecessary_brace_in_string_interps, directives_ordering
// ignore_for_file:argument_type_not_assignable, invalid_assignment
// ignore_for_file:prefer_single_quotes, prefer_generic_function_type_aliases
// ignore_for_file:comment_references

import 'dart:async';

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';
import 'package:intl/src/intl_helpers.dart';

import 'messages_de_GM.dart' as messages_de_gm;
import 'messages_en_US.dart' as messages_en_us;
import 'messages_es_MX.dart' as messages_es_mx;
import 'messages_hi_IN.dart' as messages_hi_in;
import 'messages_hy_AM.dart' as messages_hy_am;
import 'messages_it_IT.dart' as messages_it_it;
import 'messages_pt_BR.dart' as messages_pt_br;

typedef Future<dynamic> LibraryLoader();
Map<String, LibraryLoader> _deferredLibraries = {
  'de_GM': () => new Future.value(null),
  'en_US': () => new Future.value(null),
  'es_MX': () => new Future.value(null),
  'hi_IN': () => new Future.value(null),
  'hy_AM': () => new Future.value(null),
  'it_IT': () => new Future.value(null),
  'pt_BR': () => new Future.value(null),
};

MessageLookupByLibrary _findExact(String localeName) {
  switch (localeName) {
    case 'de_GM':
      return messages_de_gm.messages;
    case 'en_US':
      return messages_en_us.messages;
    case 'es_MX':
      return messages_es_mx.messages;
    case 'hi_IN':
      return messages_hi_in.messages;
    case 'hy_AM':
      return messages_hy_am.messages;
    case 'it_IT':
      return messages_it_it.messages;
    case 'pt_BR':
      return messages_pt_br.messages;
    default:
      return null;
  }
}

/// User programs should call this before using [localeName] for messages.
Future<bool> initializeMessages(String localeName) async {
  var availableLocale = Intl.verifiedLocale(
    localeName,
    (locale) => _deferredLibraries[locale] != null,
    onFailure: (_) => null);
  if (availableLocale == null) {
    return new Future.value(false);
  }
  var lib = _deferredLibraries[availableLocale];
  await (lib == null ? new Future.value(false) : lib());
  initializeInternalMessageLookup(() => new CompositeMessageLookup());
  messageLookup.addLocale(availableLocale, _findGeneratedMessagesFor);
  return new Future.value(true);
}

bool _messagesExistFor(String locale) {
  try {
    return _findExact(locale) != null;
  } catch (e) {
    return false;
  }
}

MessageLookupByLibrary _findGeneratedMessagesFor(String locale) {
  var actualLocale = Intl.verifiedLocale(locale, _messagesExistFor,
      onFailure: (_) => null);
  if (actualLocale == null) return null;
  return _findExact(actualLocale);
}
