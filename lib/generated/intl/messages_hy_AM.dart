// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a hy_AM locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'hy_AM';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Զգոն երկխոսություն"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("լաբորատորիա 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("Բարձր երթևեկության ահազանգ. Godzilla- ն սանձազերծված է"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Դատաստանի օր"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("զոմբիի ապոկալիպսիս"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Կուպերտինո երկխոսություն"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("հեռացնել"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("կտտացրեք պարզ երկխոսության համար"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Lowածր մարտկոց"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Պարզ երկխոսություն"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Ընտրեք երկխոսության տուփ"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("Մարտկոցի մնացորդի 10% -ը")
  };
}
