// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a it_IT locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'it_IT';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Finestra di dialogo Avviso"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("Laboratorio 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("Avviso di traffico intenso: Godzilla Unleashed"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Giorno del giudizio"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("Apocolypse zombie"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Finestra di dialogo Cupertino"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Respingere"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("Fare clic per la finestra di dialogo semplice"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Batteria scarica"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Finestra di dialogo semplice"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Seleziona la finestra di dialogo"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("10% di batteria rimanente")
  };
}
