// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a de_GM locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'de_GM';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Alarmdialog"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("Labor 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("Hohe Verkehrswarnung: Godzilla entfesselt"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Tag des Jüngsten Gerichts"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("Zombie-Apokalypse"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Cupertino Dialog"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Entlassen"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("Klicken Sie für den einfachen Dialog"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Niedriger Batteriestatus"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Einfacher Dialog"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Wählen Sie das Dialogfeld"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("10% der Batterie verbleiben")
  };
}
