// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es_MX locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es_MX';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Diálogo de alerta"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("Laboratorio 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("Alerta de tráfico alto: Godzilla desatado"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Día del juicio"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("Apocolipo zombi"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Diálogo de Cupertino"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Descartar"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("Haga clic para ver un cuadro de diálogo simple"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Batería baja"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Diálogo simple"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Seleccionar cuadro de diálogo"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("10% de batería restante")
  };
}
