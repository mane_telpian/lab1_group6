// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a pt_BR locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'pt_BR';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Diálogo de Alerta"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("Laboratório 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("Alerta de trânsito intenso: Godzilla Unleashed"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Dia do julgamento"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("Apocalipse zumbi"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Cupertino Diálogo"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Dispensar"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("Clique para diálogo simples"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Bateria Fraca"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Diálogo Simples"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Selecione a caixa de diálogo"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("10% de bateria restante")
  };
}
