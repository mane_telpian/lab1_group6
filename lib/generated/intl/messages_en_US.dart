// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Alert" : MessageLookupByLibrary.simpleMessage("Alert Dialog"),
    "MyAppLab1" : MessageLookupByLibrary.simpleMessage("Lab 1"),
    "alertTitle1" : MessageLookupByLibrary.simpleMessage("High Traffic Alert: Godzilla Unleashed"),
    "alertTitle2" : MessageLookupByLibrary.simpleMessage("Judgement Day"),
    "alertTitle3" : MessageLookupByLibrary.simpleMessage("Zombie Apocolypse"),
    "cAlert" : MessageLookupByLibrary.simpleMessage("Cupertino Dialog"),
    "dismiss" : MessageLookupByLibrary.simpleMessage("Dismiss"),
    "homeRaisedButtonTitle" : MessageLookupByLibrary.simpleMessage("Click for Simple Dialog"),
    "lowerBatteryTitle" : MessageLookupByLibrary.simpleMessage("Low Battery"),
    "simpleDialogTitle" : MessageLookupByLibrary.simpleMessage("Simple Dialog"),
    "simpleSelectDialogBox" : MessageLookupByLibrary.simpleMessage("Select Dialog Box"),
    "tenPercent" : MessageLookupByLibrary.simpleMessage("10% of battery remaining")
  };
}
