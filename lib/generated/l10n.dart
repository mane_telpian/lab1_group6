// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Click for Simple Dialog`
  String get homeRaisedButtonTitle {
    return Intl.message(
      'Click for Simple Dialog',
      name: 'homeRaisedButtonTitle',
      desc: '',
      args: [],
    );
  }

  /// `Lab 1`
  String get MyAppLab1 {
    return Intl.message(
      'Lab 1',
      name: 'MyAppLab1',
      desc: '',
      args: [],
    );
  }

  /// `High Traffic Alert: Godzilla Unleashed`
  String get alertTitle1 {
    return Intl.message(
      'High Traffic Alert: Godzilla Unleashed',
      name: 'alertTitle1',
      desc: '',
      args: [],
    );
  }

  /// `Dismiss`
  String get dismiss {
    return Intl.message(
      'Dismiss',
      name: 'dismiss',
      desc: '',
      args: [],
    );
  }

  /// `Simple Dialog`
  String get simpleDialogTitle {
    return Intl.message(
      'Simple Dialog',
      name: 'simpleDialogTitle',
      desc: '',
      args: [],
    );
  }

  /// `Select Dialog Box`
  String get simpleSelectDialogBox {
    return Intl.message(
      'Select Dialog Box',
      name: 'simpleSelectDialogBox',
      desc: '',
      args: [],
    );
  }

  /// `Judgement Day`
  String get alertTitle2 {
    return Intl.message(
      'Judgement Day',
      name: 'alertTitle2',
      desc: '',
      args: [],
    );
  }

  /// `Zombie Apocolypse`
  String get alertTitle3 {
    return Intl.message(
      'Zombie Apocolypse',
      name: 'alertTitle3',
      desc: '',
      args: [],
    );
  }

  /// `Low Battery`
  String get lowerBatteryTitle {
    return Intl.message(
      'Low Battery',
      name: 'lowerBatteryTitle',
      desc: '',
      args: [],
    );
  }

  /// `10% of battery remaining`
  String get tenPercent {
    return Intl.message(
      '10% of battery remaining',
      name: 'tenPercent',
      desc: '',
      args: [],
    );
  }

  /// `Cupertino Dialog`
  String get cAlert {
    return Intl.message(
      'Cupertino Dialog',
      name: 'cAlert',
      desc: '',
      args: [],
    );
  }

  /// `Alert Dialog`
  String get Alert {
    return Intl.message(
      'Alert Dialog',
      name: 'Alert',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en', countryCode: 'US'),
      Locale.fromSubtags(languageCode: 'de', countryCode: 'GM'),
      Locale.fromSubtags(languageCode: 'es', countryCode: 'MX'),
      Locale.fromSubtags(languageCode: 'hi', countryCode: 'IN'),
      Locale.fromSubtags(languageCode: 'hy', countryCode: 'AM'),
      Locale.fromSubtags(languageCode: 'it', countryCode: 'IT'),
      Locale.fromSubtags(languageCode: 'pt', countryCode: 'BR'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}