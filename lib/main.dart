//Lab 1
import 'package:lab1/generated/l10n.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:math';

void main() {runApp(MyApp());}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: S.delegate.supportedLocales,

      home: Scaffold(
        backgroundColor: Colors.white10,
        appBar: AppBar(
          title: Text('Lab 1', textAlign: TextAlign.center,),
          backgroundColor: Colors.grey,
        ),
        body: Stack(
          children: <Widget>[
            Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("images/apoco.jpg", ),
                fit: BoxFit.cover,
                    ),
                  ),
                ),
            Home(),
          ],
        )
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: <Widget>[
          Padding(padding: const EdgeInsets.symmetric(vertical: 100.0, horizontal: 1000.0)),
          RaisedButton(
          child: Text(S.of(context).homeRaisedButtonTitle),
          onPressed: () {
            showDialog(
                context: context,
                builder: (_) => simple(),);
              },
            ),
          ],
        );
    }
}

//Alert Dialog
//----------------------------------------
class alert extends StatefulWidget {
  @override
  _alertState createState() => _alertState();
}

class _alertState extends State<alert> {
  String alertName, alertTitle;

  @override
  String randomize() {
    Random rnd;
    int min = 1;
    int max = 4;
    rnd = new Random();
    int r = min + rnd.nextInt(max - min);

    String pictureName;
    switch (r) {
      case 1: {pictureName = 'images/godzilla.jpg';}
        break;

      case 2: {pictureName = 'images/robots.jpg';}
        break;

      case 3: {pictureName = 'images/zombies.jpg';}
        break;

      default: {pictureName = 'images/zombies.jpg';}
        break;
    }
    return pictureName;
  }

  String pickAlertName(String alertName) {
    String alertTitle = 'images/godzilla.jpg';

    if (alertName == 'images/godzilla.jpg')
      alertTitle = S.of(context).alertTitle1;

    else if (alertName == 'images/robots.jpg')
      alertTitle = S.of(context).alertTitle2;

    else if (alertName == 'images/zombies.jpg')
      alertTitle = S.of(context).alertTitle3;

    return alertTitle;
  }

  @override
  void _openHome() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext context) => MyApp()));
  }

  Widget build(BuildContext context) {
    alertName = randomize(); //Image name
    alertTitle  = pickAlertName(alertName); //Alert Headline

    return Scaffold(
      body: AlertDialog(
        title: Text('$alertTitle'),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Image.asset(alertName, width: 450, height: 240, fit: BoxFit.cover,),
              SizedBox(height: 10,),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(S.of(context).dismiss),
            onPressed: () {
              _openHome();
            },
          )
        ],
      ),
    );
  }
}

//Cupertino Dialog
//----------------------------------------
class cupertino extends StatefulWidget {
  @override
  _cupertinoState createState() => _cupertinoState();
}

class _cupertinoState extends State<cupertino> {
  @override

  void _openHome() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => MyApp()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: CupertinoAlertDialog(
        title: Text(S.of(context).lowerBatteryTitle),
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Text(S.of(context).tenPercent),
              SizedBox(height: 10,),
            ],
          ),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(S.of(context).dismiss),
            onPressed: () { _openHome();},
          )
        ],
      ),
    );
  }
}

//Simple Dialog
//----------------------------------------
class simple extends StatefulWidget {
  @override
  _simpleState createState() => _simpleState();
}

class _simpleState extends State<simple> {
  @override

  void _openAlert() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => alert()));
  }

  void _openCupertino() {
    Navigator.of(context).popUntil((route) => route.isFirst);
    Navigator.pushReplacement(context, MaterialPageRoute(builder: (BuildContext context) => cupertino()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: SimpleDialog(
        title: Text(S.of(context).simpleDialogTitle, textAlign: TextAlign.center),
        children: <Widget>[
          Text(S.of(context).simpleSelectDialogBox, textAlign: TextAlign.center),
          SizedBox(height: 15,),
          SimpleDialogOption(
            onPressed: () { _openAlert();},
            child: Text(S.of(context).Alert),
          ),

          SimpleDialogOption(
            onPressed: () { _openCupertino();},
            child: Text(S.of(context).cAlert),
          ),

          SimpleDialogOption(
            onPressed: () {Navigator.of(context).pop();},
            child: Text(S.of(context).dismiss, textAlign: TextAlign.center),
          ),
        ],
      ),
    );
  }
}